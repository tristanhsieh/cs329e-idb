import json
from models import app, db, Channel, Playlist, Video

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


def create_channels():
    channel = load_json('./jsons/channels.json')

    for oneChannel in channel:
        channel_link_id = oneChannel['Link']
        name = oneChannel['Name']
        date_joined = oneChannel['Date joined']
        number_subscribers = oneChannel['Number of subscribers']
        number_videos = oneChannel['Number of videos']
        number_playlists = oneChannel['Number of playlists']
        number_views = oneChannel['Number of views']
        image = oneChannel['Image']
        #i dont think the below has been implemented in models.py
        playlist_links = oneChannel['Playlist links']
        video_links = oneChannel['Video links']

        newChannel = Channel(channel_link_id = channel_link_id, name = name, date_joined = date_joined, number_subscribers = number_subscribers, number_videos = number_videos, number_playlists = number_playlists, number_views = number_views, image= image, playlist_links = playlist_links, video_links = video_links) #leaving out 'Playlist links' section of json and 'Video links' for now

        db.session.add(newChannel)
        db.session.commit()

def create_videos():
    video = load_json('./jsons/videos.json')
    for oneVideo in video: #sketch??

        video_link_id = oneVideo['Link']
        name = oneVideo['Name']
        date_published = oneVideo['Date published']
        number_views = oneVideo['Number of views']
        number_comments = oneVideo['Number of comments']
        number_likes = oneVideo['Number of likes']
        number_dislikes = oneVideo['Number of dislikes']
        embed_link = oneVideo['Embed link']
        #not throwing in channel_links and playlist_links relations for now
        playlist_links = oneVideo['Playlist links']
        channel_links = oneVideo['Channel links']

        newVideo = Video(video_link_id = video_link_id, name = name, date_published = date_published, number_views = number_views, number_comments = number_comments, number_likes = number_likes, number_dislikes = number_dislikes, embed_link = embed_link, playlist_links = playlist_links, channel_links = channel_links)
        db.session.add(newVideo)
        db.session.commit()

def create_playlists():
    playlist = load_json('./jsons/playlists.json')

    for onePlaylist in playlist:
        playlist_link_id = onePlaylist['Link']
        name = onePlaylist['Name']
        date_last_updated = onePlaylist['Date last updated']
        number_of_videos = onePlaylist['Number of videos']
        number_of_views = onePlaylist['Number of views']
        embed_link = onePlaylist['Embed Link']
        channel_links = onePlaylist['Channel links']
        video_links = onePlaylist['Video links']
        #need to implement the 2 relations channel_links and video_links

        new_playlist = Playlist(playlist_link_id = playlist_link_id, name = name, date_last_updated = date_last_updated, number_of_videos = number_of_videos, number_of_views = number_of_views, embed_link = embed_link, channel_links = channel_links, video_links = video_links)
        db.session.add(new_playlist)
        db.session.commit()


create_channels()
create_playlists()
create_videos()


