from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

#all the data i intend to store as integers is in quotes, which leads me to think
#ti won't be accepted as integer so easily. I may have to convert it into an integer
#in python when i load it into the db, or i may have to remove the quotes in the json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:admin@127.0.0.1:5432/youtube')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True #suppress warning message
db = SQLAlchemy(app)

#Channel-Playlist = db.Table('Channel-Playlist',
 #   db.Column('channel_link_id', db.String(80), db.ForeignKey('channel.channel_link_id')),
  #  db.Column('playlist_link_id', db.String(80), db.ForeignKey('playlist.playlist_link_id'))
   # )

class Playlist(db.Model):
    __tablename__ = 'playlist'

    #the below should only begin to store as ids the arg after 'list=' of the url in playlist json data
    playlist_link_id = db.Column(db.String(150),primary_key= True)
    name = db.Column(db.String(150),nullable=False)
    #this is tentative; the json is a string with words, i want it to be stored in postgres
    #as a date type
    date_last_updated = db.Column(db.String(150),nullable=False)
    number_of_videos = db.Column(db.Integer,nullable=False)
    number_of_views = db.Column(db.BigInteger,nullable=False)
    embed_link = (db.Column(db.String(150),nullable=False))
    channel_links = db.Column(db.ARRAY(db.String(300)),nullable=False)
    video_links = db.Column(db.ARRAY(db.String(300)),nullable=False)
    #below, dont forget 2 more relations: channel links & video links




#one to many : 1 channel has many videos
#multiple videos belong to one channel
#many to many: paylists and channels
class Channel(db.Model):
    __tablename__ = 'channel'

    #for the below, i will only use portion of json after 'channel/'
    channel_link_id = db.Column(db.String(150),primary_key= True)
    name = db.Column(db.String(150),nullable=False)
    #date publicshed needs to be adjusted so it can be a date object in postgres
    date_joined = db.Column(db.String(150),nullable=False)
    number_subscribers = db.Column(db.BigInteger,nullable=False)
    number_videos = db.Column(db.Integer,nullable=False)
    number_playlists = db.Column(db.Integer,nullable=False)
    number_views = db.Column(db.BigInteger,nullable=False)
    image= db.Column(db.String(150),nullable=False)
    playlist_links = db.Column(db.ARRAY(db.String(300)),nullable=False)
    video_links = db.Column(db.ARRAY(db.String(300)),nullable=False)
    #2 more relations to be represented later
    #playlists_in = db.relationship('Playlist', secondary='Channel-Playlist', backref=db.backref('member_channels', lazy = 'dynamic') )
    #making it lazy ensures all the data doesnt get put into the object at once, as a channel can be part of many playlists

class Video(db.Model):
    __tablename__ = 'video'

    #i plan on using the portion after 'watch?v=' for link_id
    video_link_id = db.Column(db.String(150),primary_key = True)
    name = db.Column(db.String(150),nullable=False)
    #date publicshed needs to be adjusted so it can be a date object in postgres
    date_published = db.Column(db.String(150),nullable=False)
    number_views = db.Column(db.BigInteger,nullable=False)
    number_comments = db.Column(db.BigInteger,nullable=False)
    number_likes = db.Column(db.BigInteger,nullable=False)
    number_dislikes = db.Column(db.BigInteger,nullable=False)
    embed_link = db.Column(db.String(150),nullable=False)
    channel_links = db.Column(db.ARRAY(db.String(300)),nullable=False)
    playlist_links = db.Column(db.ARRAY(db.String(300)),nullable=False)
    #2 more relations; one channel has many videos; one video belongs to a channel
    #many to many: one video can be in many playlists; a playlist has multiple videos

db.create_all()
#end;;;;; THIS IS A ROUGH DRAFT THAT STILL NEEDS THE RELATIONSHIPS IMPLEMENTED; NOT FUNCTIONAL AT THIS POINT
