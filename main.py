from flask import Flask, render_template, request, jsonify
from create_db import app, db, Channel, create_channels, Video, create_videos, Playlist, create_playlists, load_json
import json
#from search import main
#import request

idpb = load_json('./jsons/idpb.json')

# The route decorator '@app.route()' maps a function to a route on your website.
# decorators are used to map a function, index(), to a web page, / or
# i.e., when someone types in the home address of the web site,
# flask will run the function index()
# summary: type in a URL, flask check the URL, finds the associate function with it, runs the
# function, collect responses, and send back the results to the browser
@app.route('/search',methods = ['POST'])
def search():
    print(request.form)
    searchQ = request.form['searchValNav']
    searchType = request.form['type']
    searchV = Video.query.filter(Video.name.ilike('%' + searchQ + '%')).all()
    searchC = Channel.query.filter(Channel.name.ilike('%' + searchQ + '%')).all()
    searchP = Playlist.query.filter(Playlist.name.ilike('%' + searchQ + '%')).all()
    
    newList = []
    if(searchType == "videos" or searchType == "everything"):
        count = 0
        for i in searchV:
            appendDic = {}
            appendDic["name"] = i.name
            appendDic["link"] = idpb[i.name]
            newList.append(appendDic)
            count += 1;
    
    if(searchType == "channels" or searchType == "everything"):
        count = 0
        print(idpb["Erica's World"])
        for i in searchC:
            appendDic = {}
            appendDic["name"] = i.name
            print(i.name)
            appendDic["link"] = idpb[i.name]
            newList.append(appendDic)
            count += 1;
    
    if(searchType == "playlists" or searchType == "everything"):
        count = 0
        for i in searchP:
            appendDic = {}
            appendDic["name"] = i.name
            appendDic["link"] = idpb[i.name]
            newList.append(appendDic)
            count += 1;
    
    return jsonify(newList)

@app.errorhandler(404)
def page_not_found(e):
    #note that we set 404 status explicitly
    return render_template('404.html'), 404

@app.route('/')
def index():
    viddb = db.session.query(Video).all()
    return render_template('splash.html')
    
@app.route('/about')
def about():
    return render_template('about.html')
    
@app.route('/videos')
def videos():
    return render_template('videos.html')

@app.route('/channels')
def channels():
    return render_template('channels.html')

@app.route('/playlists')
def playlists():
    return render_template('playlists.html')
@app.route('/videos2')
def videos2():
    return render_template('videos2.html')

@app.route('/channels2')
def channels2():
    return render_template('channels2.html')

@app.route('/playlists2')
def playlists2():
    return render_template('playlists2.html')

@app.route('/video_search/<name>')
def video_search(name):
    result = Video.query.filter(Video.name.ilike('%' + name + '%')).first_or_404()
    return render_template('videotemplate.html', vid = result, idpb = idpb)

@app.route('/video1')
def video1():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[0], idpb = idpb)

@app.route('/video2')
def video2():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[1], idpb = idpb)

@app.route('/video3')
def video3():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[2], idpb = idpb)

@app.route('/video4')
def video4():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[3], idpb = idpb)

@app.route('/video5')
def video5():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[4], idpb = idpb)

@app.route('/video6')
def video6():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[5], idpb = idpb)

@app.route('/video7')
def video7():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[6], idpb = idpb)

@app.route('/video8')
def video8():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[7], idpb = idpb)

@app.route('/video9')
def video9():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[8], idpb = idpb)

@app.route('/video10')
def video10():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[9], idpb = idpb)

@app.route('/video11')
def video11():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[10], idpb = idpb)

@app.route('/video12')
def video12():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[11], idpb = idpb)

@app.route('/video13')
def video13():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[12], idpb = idpb)

@app.route('/video14')
def video14():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[13], idpb = idpb)

@app.route('/video15')
def video15():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[14], idpb = idpb)

@app.route('/video16')
def video16():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[15], idpb = idpb)

@app.route('/video17')
def video17():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[16], idpb = idpb)

@app.route('/video18')
def video18():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[17], idpb = idpb)

@app.route('/video19')
def video19():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[18], idpb = idpb)

@app.route('/video20')
def video20():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[19], idpb = idpb)

@app.route('/video21')
def video21():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[20], idpb = idpb)

@app.route('/video22')
def video22():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[21], idpb = idpb)

@app.route('/video23')
def video23():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[22], idpb = idpb)

@app.route('/video24')
def video24():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[23], idpb = idpb)

@app.route('/video25')
def video25():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[24], idpb = idpb)

@app.route('/video26')
def video26():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[25], idpb = idpb)

@app.route('/video27')
def video27():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[26], idpb = idpb)

@app.route('/video28')
def video28():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[27], idpb = idpb)

@app.route('/video29')
def video29():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[28], idpb = idpb)

@app.route('/video30')
def video30():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[29], idpb = idpb)

@app.route('/video31')
def video31():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[30], idpb = idpb)

@app.route('/video32')
def video32():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[31], idpb = idpb)

@app.route('/video33')
def video33():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[32], idpb = idpb)

@app.route('/video34')
def video34():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[33], idpb = idpb)

@app.route('/video35')
def video35():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[34], idpb = idpb)

@app.route('/video36')
def video36():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[35], idpb = idpb)

@app.route('/video37')
def video37():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[36], idpb = idpb)

@app.route('/video38')
def video38():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[37], idpb = idpb)

@app.route('/video39')
def video39():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[38], idpb = idpb)

@app.route('/video40')
def video40():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[39], idpb = idpb)

@app.route('/video41')
def video41():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[40], idpb = idpb)

@app.route('/video42')
def video42():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[41], idpb = idpb)

@app.route('/video43')
def video43():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[42], idpb = idpb)

@app.route('/video44')
def video44():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[43], idpb = idpb)

@app.route('/video45')
def video45():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[44], idpb = idpb)

@app.route('/video46')
def video46():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[45], idpb = idpb)

@app.route('/video47')
def video47():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[46], idpb = idpb)

@app.route('/video48')
def video48():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[47], idpb = idpb)

@app.route('/video49')
def video49():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[48], idpb = idpb)

@app.route('/video50')
def video50():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[49], idpb = idpb)

@app.route('/video51')
def video51():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[50], idpb = idpb)

@app.route('/video52')
def video52():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[51], idpb = idpb)

@app.route('/video53')
def video53():
    viddb = db.session.query(Video).all()
    return render_template('videotemplate.html', vid = viddb[52], idpb = idpb)

@app.route('/channel1')
def channel1():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[0], idpb = idpb)

@app.route('/channel2')
def channel2():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[1], idpb = idpb)

@app.route('/channel3')
def channel3():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[2], idpb = idpb)

@app.route('/channel4')
def channel4():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[3], idpb = idpb)

@app.route('/channel5')
def channel5():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[4], idpb = idpb)

@app.route('/channel6')
def channel6():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[5], idpb = idpb)

@app.route('/channel7')
def channel7():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[6], idpb = idpb)

@app.route('/channel8')
def channel8():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[7], idpb = idpb)

@app.route('/channel9')
def channel9():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[8], idpb = idpb)

@app.route('/channel10')
def channel10():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[9], idpb = idpb)

@app.route('/channel11')
def channel11():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[10], idpb = idpb)

@app.route('/channel12')
def channel12():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[11], idpb = idpb)

@app.route('/channel13')
def channel13():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[12], idpb = idpb)

@app.route('/channel14')
def channel14():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[13], idpb = idpb)

@app.route('/channel15')
def channel15():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[14], idpb = idpb)

@app.route('/channel16')
def channel16():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[15], idpb = idpb)

@app.route('/channel17')
def channel17():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[16], idpb = idpb)

@app.route('/channel18')
def channel18():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[17], idpb = idpb)

@app.route('/channel19')
def channel19():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[18], idpb = idpb)

@app.route('/channel20')
def channel20():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[19], idpb = idpb)

@app.route('/channel21')
def channel21():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[20], idpb = idpb)

@app.route('/channel22')
def channel22():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[21], idpb = idpb)

@app.route('/channel23')
def channel23():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[22], idpb = idpb)

@app.route('/channel24')
def channel24():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[23], idpb = idpb)

@app.route('/channel25')
def channel25():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[24], idpb = idpb)

@app.route('/channel26')
def channel26():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[25], idpb = idpb)

@app.route('/channel27')
def channel27():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[26], idpb = idpb)

@app.route('/channel28')
def channel28():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[27], idpb = idpb)

@app.route('/channel29')
def channel29():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[28], idpb = idpb)

@app.route('/channel30')
def channel30():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[29], idpb = idpb)

@app.route('/channel31')
def channel31():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[30], idpb = idpb)

@app.route('/channel32')
def channel32():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[31], idpb = idpb)

@app.route('/channel33')
def channel33():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[32], idpb = idpb)

@app.route('/channel34')
def channel34():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[33], idpb = idpb)

@app.route('/channel35')
def channel35():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[34], idpb = idpb)

@app.route('/channel36')
def channel36():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[35], idpb = idpb)

@app.route('/channel37')
def channel37():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[36], idpb = idpb)

@app.route('/channel38')
def channel38():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[37], idpb = idpb)

@app.route('/channel39')
def channel39():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[38], idpb = idpb)

@app.route('/channel40')
def channel40():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[39], idpb = idpb)

@app.route('/channel41')
def channel41():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[40], idpb = idpb)

@app.route('/channel42')
def channel42():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[41], idpb = idpb)

@app.route('/channel43')
def channel43():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[42], idpb = idpb)

@app.route('/channel44')
def channel44():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[43], idpb = idpb)

@app.route('/channel45')
def channel45():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[44], idpb = idpb)

@app.route('/channel46')
def channel46():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[45], idpb = idpb)

@app.route('/channel47')
def channel47():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[46], idpb = idpb)

@app.route('/channel48')
def channel48():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[47], idpb = idpb)

@app.route('/channel49')
def channel49():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[48], idpb = idpb)

@app.route('/channel50')
def channel50():
    chandb = db.session.query(Channel).all()
    return render_template('channeltemplate.html', chan = chandb[49], idpb = idpb)

@app.route('/playlist1')
def playlist1():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[0], idpb = idpb)

@app.route('/playlist2')
def playlist2():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[1], idpb = idpb)

@app.route('/playlist3')
def playlist3():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[2], idpb = idpb)

@app.route('/playlist4')
def playlist4():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[3], idpb = idpb)

@app.route('/playlist5')
def playlist5():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[4], idpb = idpb)

@app.route('/playlist6')
def playlist6():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[5], idpb = idpb)

@app.route('/playlist7')
def playlist7():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[6], idpb = idpb)

@app.route('/playlist8')
def playlist8():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[7], idpb = idpb)

@app.route('/playlist9')
def playlist9():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[8], idpb = idpb)

@app.route('/playlist10')
def playlist10():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[9], idpb = idpb)

@app.route('/playlist11')
def playlist11():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[10], idpb = idpb)

@app.route('/playlist12')
def playlist12():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[11], idpb = idpb)

@app.route('/playlist13')
def playlist13():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[12], idpb = idpb)

@app.route('/playlist14')
def playlist14():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[13], idpb = idpb)

@app.route('/playlist15')
def playlist15():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[14], idpb = idpb)

@app.route('/playlist16')
def playlist16():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[15], idpb = idpb)

@app.route('/playlist17')
def playlist17():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[16], idpb = idpb)

@app.route('/playlist18')
def playlist18():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[17], idpb = idpb)

@app.route('/playlist19')
def playlist19():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[18], idpb = idpb)

@app.route('/playlist20')
def playlist20():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[19], idpb = idpb)

@app.route('/playlist21')
def playlist21():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[20], idpb = idpb)

@app.route('/playlist22')
def playlist22():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[21], idpb = idpb)

@app.route('/playlist23')
def playlist23():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[22], idpb = idpb)

@app.route('/playlist24')
def playlist24():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[23], idpb = idpb)

@app.route('/playlist25')
def playlist25():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[24], idpb = idpb)

@app.route('/playlist26')
def playlist26():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[25], idpb = idpb)

@app.route('/playlist27')
def playlist27():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[26], idpb = idpb)

@app.route('/playlist28')
def playlist28():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[27], idpb = idpb)

@app.route('/playlist29')
def playlist29():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[28], idpb = idpb)

@app.route('/playlist30')
def playlist30():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[29], idpb = idpb)

@app.route('/playlist31')
def playlist31():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[30], idpb = idpb)

@app.route('/playlist32')
def playlist32():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[31], idpb = idpb)

@app.route('/playlist33')
def playlist33():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[32], idpb = idpb)

@app.route('/playlist34')
def playlist34():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[33], idpb = idpb)

@app.route('/playlist35')
def playlist35():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[34], idpb = idpb)

@app.route('/playlist36')
def playlist36():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[35], idpb = idpb)

@app.route('/playlist37')
def playlist37():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[36], idpb = idpb)

@app.route('/playlist38')
def playlist38():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[37], idpb = idpb)

@app.route('/playlist39')
def playlist39():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[38], idpb = idpb)

@app.route('/playlist40')
def playlist40():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[39], idpb = idpb)

@app.route('/playlist41')
def playlist41():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[40], idpb = idpb)

@app.route('/playlist42')
def playlist42():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[41], idpb = idpb)

@app.route('/playlist43')
def playlist43():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[42], idpb = idpb)

@app.route('/playlist44')
def playlist44():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[43], idpb = idpb)

@app.route('/playlist45')
def playlist45():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[44], idpb = idpb)

@app.route('/playlist46')
def playlist46():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[45], idpb = idpb)

@app.route('/playlist47')
def playlist47():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[46], idpb = idpb)

@app.route('/playlist48')
def playlist48():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[47], idpb = idpb)

@app.route('/playlist49')
def playlist49():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[48], idpb = idpb)

@app.route('/playlist50')
def playlist50():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[49], idpb = idpb)

@app.route('/playlist51')
def playlist51():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[50], idpb = idpb)

@app.route('/playlist52')
def playlist52():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[51], idpb = idpb)

@app.route('/playlist53')
def playlist53():
    pldb = db.session.query(Playlist).all()
    return render_template("playlisttemplate.html", pl = pldb[52], idpb = idpb)

@app.route('/test.html')
def test():
    return render_template("test.html")

# if main.py is run directly, i.e., as the main module, it will be assigned the value main
# and if it's main go ahead and run the application.
# if this application is imported, then the __name__ is no longer __main__ and
# the code, app.run(), will not be executed
if __name__ == "__main__":
    app.run()

