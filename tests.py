import json
from io import StringIO
from unittest import main, TestCase

from create_db import app, db, Channel, Playlist, Video, create_channels, create_videos, create_playlists

class TestIDB (TestCase):
    def test_playlist_get(self):
        r = db.session.query(Playlist).all()
        self.assertEqual(r[0].name, "important videos")
        
    def test_playlist_add(self):
        w = Playlist(
            playlist_link_id = "test",
            name = "test",
            date_last_updated = "test",
            number_of_videos = 0,
            number_of_views = 0,
            embed_link = "test",
            channel_links = ["test"],
            video_links = ["test"]
                    )
        
        db.session.add(w)
        db.session.commit()
        
        r = db.session.query(Playlist).filter_by(name = "test").one()
        self.assertEqual(str(r.name), "test")
        
        db.session.query(Playlist).filter_by(name = "test").delete()
        db.session.commit()
        
    def test_playlist_load(self):
        with open('./jsons/playlists.json') as file:
            r = json.load(file)
        self.assertEqual(r[0]["Name"], "important videos")
        file.close()

    def test_channel_get(self):
        r = db.session.query(Channel).all()
        self.assertEqual(r[0].name, "kierancaspian")
        
    def test_channel_add(self):
        w = Channel(
            channel_link_id = "test",
            name = "test",
            date_joined = "test",
            number_subscribers = 0,
            number_videos = 0,
            number_playlists = 0,
            number_views = 0,
            image= "test",
            playlist_links = ["test"],
            video_links = ["test"]
                    )
        
        db.session.add(w)
        db.session.commit()
        
        r = db.session.query(Channel).filter_by(name = "test").one()
        self.assertEqual(str(r.name), "test")
        
        db.session.query(Channel).filter_by(name = "test").delete()
        db.session.commit()
        
    def test_channel_load(self):
        with open('./jsons/channels.json') as file:
            r = json.load(file)
        self.assertEqual(r[0]["Name"], "kierancaspian")
        file.close()

    def test_video_get(self):
        r = db.session.query(Video).all()
        self.assertEqual(r[0].name, "Yee")
        
    def test_video_add(self):
        w = Video(
            video_link_id = "test",
            name = "test",
            date_published = "test",
            number_views = 0,
            number_comments = 0,
            number_likes = 0,
            number_dislikes = 0,
            embed_link = "test",
            channel_links = ["test"],
            playlist_links = ["test"]
                    )
        
        db.session.add(w)
        db.session.commit()
        
        r = db.session.query(Video).filter_by(name = "test").one()
        self.assertEqual(str(r.name), "test")
        
        db.session.query(Video).filter_by(name = "test").delete()
        db.session.commit()
        
    def test_video_load(self):
        with open('./jsons/videos.json') as file:
            r = json.load(file)
        self.assertEqual(r[0]["Name"], "Yee")
        file.close()

if __name__ == '__main__':
    main()
